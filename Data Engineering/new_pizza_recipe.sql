with orders_clean as(
					select order_id, customer_id, pizza_id
					,CASE when exclusions is null or exclusions in ('null','') then null
						  else concat('-',replace(exclusions,', ',', -')) end exclusions
					,CASE when extras is null or extras in ('null','') then null
						  else extras end extras
					,order_time
					from dbo.Orders)
, ordersxpizza as(
					select *
					, case when o.extras is not null and o.exclusions is not null then concat(p.ingredients, ', ', o.extras, ', ',o.exclusions)
						   when o.extras is null and o.exclusions is not null then concat(p.ingredients, ', ', o.exclusions)
						   when o.extras is not null and o.exclusions is null then concat(p.ingredients, ', ', o.extras)
						   else p.ingredients
					  end concatenado
					from orders_clean o inner join [dbo].[Pizza] p on o.pizza_id = p.id)
, agrupado as (
					select order_id , STRING_AGG(concatenado,',') ingredientes
					from ordersxpizza
					group by order_id)
select * 
from agrupado p
outer apply 
(
    SELECT 
    CAST(spl.value AS INT) AS ingredient
    FROM STRING_SPLIT(
        (SELECT _p.ingredientes 
		FROM agrupado AS _p WHERE _p.order_id = p.order_id), ',') AS spl
 )s